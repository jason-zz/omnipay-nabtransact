# Omnipay: NAB Transact

**Internetrix custom NAB Transact driver for the Omnipay PHP payment processing library**

[Omnipay](https://github.com/thephpleague/omnipay) is a framework agnostic, multi-gateway payment
processing library for PHP 5.3+. This package implements NAB Transact support for Omnipay.

NAB Transact gateway API is similar with SecurePay. This module is forked from [Omnipay-SecurePay](https://github.com/thephpleague/omnipay-securepay).

## Installation

Omnipay is installed via [Composer](http://getcomposer.org/). To install, simply add it
to your `composer.json` file:

```json
{
	"repositories" : [
		{
			"type" : "git",
			"url" : "xxxxx",
			"name" : "internetrix/omnipay-nabtransact"
		}
	],
    "require": {
        "internetrix/omnipay-nabtransact": "~1.0"
    }
}
```

And run composer to update your dependencies:

    $ curl -s http://getcomposer.org/installer | php
    $ php composer.phar update