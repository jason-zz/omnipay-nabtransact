<?php

namespace Omnipay\NABTransact\Message;

/**
 * NABTransact Direct Post Purchase Request
 */
class DirectPostPurchaseRequest extends DirectPostAuthorizeRequest
{
	static public $txnType = '2';
}
