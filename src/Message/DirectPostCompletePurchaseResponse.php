<?php

namespace Omnipay\NABTransact\Message;

use Omnipay\Common\Message\AbstractResponse;

/**
 * NABTransact Direct Post Complete Purchase Response
 */
class DirectPostCompletePurchaseResponse extends AbstractResponse
{
    public function isSuccessful()
    {
        return isset($this->data['summarycode']) && $this->data['summarycode'] == 1;
    }

    public function getMessage()
    {
        if (isset($this->data['restext'])){
        	return $this->data['restext'];
        }
    }

    public function getCode()
    {
        if (isset($this->data['rescode'])){
        	return $this->data['rescode'];
        }
    }

    public function getTransactionReference()
    {
        if (isset($this->data['txnid'])){
        	return $this->data['txnid'];
        }
    }
    
    public function getStresCode ()
    {
    	if (isset($this->data['strescode'])){
    		return $this->data['strescode'];
    	}
    }
    
    public function getStresText()
    {
    	if (isset($this->data['strestext'])){
    		return $this->data['strestext'];
    	}
    }
    
    public function getToken()
    {
    	if (isset($this->data['token'])){
    		return $this->data['token'];
    	}
    }
    
    public function getMaskedCardNumber()
    {
    	if (isset($this->data['pan'])){
    		return $this->data['pan'];
    	}
    }
    
    public function getSetDate()
    {
    	if (isset($this->data['settdate'])){
    		return $this->data['settdate'];
    	}
    }
    
    public function getExpiryMonth()
    {
    	if (isset($this->data['expirydate'])){
    		//fist 1 or 2 digits is month
    		if(strlen($this->data['expirydate']) == 6){
    			//6 digits
    			return substr($this->data['expirydate'], 0, 2);
    		}else{
    			//5 digits
    			return substr($this->data['expirydate'], 0, 1);
    		}
    	}
    }

    public function getExpiryYear()
    {
    	if (isset($this->data['expirydate'])){
    		//last 4 digits is year
    		return substr($this->data['expirydate'], -4, 4);
    	}
    }
    
    
}
