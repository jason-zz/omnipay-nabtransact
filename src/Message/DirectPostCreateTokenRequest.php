<?php

namespace Omnipay\NABTransact\Message;

/**
 * NABTransact Direct Post Create Token Request
 */
class DirectPostCreateTokenRequest extends DirectPostAuthorizeRequest
{
	static public $txnType = '8';
	
	public function getData()
	{
		$data = parent::getData();
		
		$data['EPS_TXNTYPE'] = self::$txnType;
		$data['EPS_STORE'] = 'TRUE';
		$data['EPS_STORETYPE'] = 'TOKEN';
	
		return $data;
	}
	
	protected function generateHashDataArray(array $data){
		//Token Only
		$processedData = array(
			$data['EPS_MERCHANT'],
			$this->getTransactionPassword(),
			self::$txnType,
			'TOKEN',
			$data['EPS_REFERENCEID'],
			$data['EPS_TIMESTAMP'],
		);
	
		return $processedData;
	}
	
}
