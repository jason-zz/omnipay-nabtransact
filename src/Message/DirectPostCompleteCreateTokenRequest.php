<?php

namespace Omnipay\NABTransact\Message;

use Omnipay\Common\Exception\InvalidRequestException;

/**
 * NABTransact Direct Post Complete Purchase Request
 */
class DirectPostCompleteCreateTokenRequest extends DirectPostAbstractRequest
{
    public function getData()
    {
        $data = $this->httpRequest->query->all();

        if($this->httpRequest->query->get('strescode') != '00'){
        	throw new InvalidRequestException($this->httpRequest->query->get('strestext'));
        }

        return $data;
    }
    
    protected function generateHashDataArray(array $data){
    	$processedData = array(
    		$data['merchant'],
    		$this->getTransactionPassword(),
    		'TOKEN',
    		$data['refid'],
            $data['timestamp'],
            $data['summarycode']
    	);
    
    	return $processedData;
    }
    
    public function generateFingerprint(array $data)
    {
    	$hash = implode('|', $this->generateHashDataArray($data));
    
    	return sha1($hash);
    }

    public function sendData($data)
    {
        return $this->response = new DirectPostCompleteCreateTokenResponse($this, $data);
    }
}
