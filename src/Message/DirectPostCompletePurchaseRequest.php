<?php

namespace Omnipay\NABTransact\Message;

use Omnipay\Common\Exception\InvalidRequestException;

/**
 * NABTransact Direct Post Complete Purchase Request
 */
class DirectPostCompletePurchaseRequest extends DirectPostAbstractRequest
{
    public function getData()
    {
        $data = $this->httpRequest->query->all();

        if ($this->generateResponseFingerprint($data) !== $this->httpRequest->query->get('fingerprint')) {
            throw new InvalidRequestException('Invalid fingerprint');
        }

        return $data;
    }

    public function generateResponseFingerprint($data)
    {
    	if($this->getToken()){
    		//pay and get token for this card.
    		$processedData = array(
    			$data['merchant'],
    			$this->getTransactionPassword(),
    			'TOKEN',
    			$data['refid'],
    			$data['timestamp'],
    			$data['summarycode'],
    		);
    	}else{
    		$processedData = array(
    			$data['merchant'],
    			$this->getTransactionPassword(),
    			$data['refid'],
    			$this->getAmount(),
    			$data['timestamp'],
    			$data['summarycode'],
    		);
    	}
    	
    	$hash = implode('|', $processedData);

        return sha1($hash);
    }

    public function sendData($data)
    {
        return $this->response = new DirectPostCompletePurchaseResponse($this, $data);
    }
}
