<?php

namespace Omnipay\NABTransact\Message;

use Omnipay\Common\Message\AbstractResponse;

/**
 * NABTransact Direct Post Complete Purchase Response
 */
class DirectPostCompleteCreateTokenResponse extends AbstractResponse
{
    public function isSuccessful()
    {
        return isset($this->data['summarycode']) && $this->data['summarycode'] == 1;
    }

    public function getMessage()
    {
        if (isset($this->data['strestext'])) {
            return $this->data['strestext'];
        }
    }

    public function getCode()
    {
        if (isset($this->data['strescode'])) {
            return $this->data['strescode'];
        }
    }

    public function getTransactionReference()
    {
        if (isset($this->data['refid'])) {
            return $this->data['refid'];
        }
    }
    
    public function getToken()
    {
    	if (isset($this->data['token'])) {
    		return $this->data['token'];
    	}
    }
}
