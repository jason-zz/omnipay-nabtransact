<?php

namespace Omnipay\NABTransact\Message;

/**
 * NABTransact Direct Post Authorize Request
 */

class DirectPostAuthorizeRequest extends DirectPostAbstractRequest
{
    static public $txnType = '2';

    public function getData()
    {
        $this->validate('amount', 'returnUrl', 'card');

        $data = array();
        $data['EPS_MERCHANT'] = $this->getMerchantId();
        $data['EPS_TXNTYPE'] = self::$txnType;
        $data['EPS_IP'] = $this->getClientIp();
        $data['EPS_AMOUNT'] = $this->getAmount();
        $data['EPS_REFERENCEID'] = $this->getTransactionId();
        $data['EPS_TIMESTAMP'] = gmdate('YmdHis');
        $data['EPS_FINGERPRINT'] = $this->generateFingerprint($data);
        $data['EPS_RESULTURL'] = $this->getReturnUrl();
        $data['EPS_CALLBACKURL'] = $this->getReturnUrl();
        $data['EPS_REDIRECT'] = 'TRUE';
        $data['EPS_CURRENCY'] = $this->getCurrency();

        if($this->getDoFetchToken()){
        	$data['EPS_STORE'] = 'TRUE';
        	$data['EPS_STORETYPE'] = 'TOKEN';
        }

        $data = array_replace($data, $this->getCardData());

        return $data;
    }
    
    protected function generateHashDataArray(array $data){
    	$processedData = array(
    		$data['EPS_MERCHANT'],
    		$this->getTransactionPassword(),
    		$data['EPS_TXNTYPE'],
    		$data['EPS_REFERENCEID'],
    		$data['EPS_AMOUNT'],
    		$data['EPS_TIMESTAMP'],
    	);
    	 
    	return $processedData;
    }

    public function generateFingerprint(array $data)
    {
    	$hash = implode('|', $this->generateHashDataArray($data));

        return sha1($hash);
    }

    public function sendData($data)
    {
        return $this->response = new DirectPostAuthorizeResponse($this, $data, $this->getEndpoint());
    }

    protected function getCardData()
    {
        $this->getCard()->validate();

        $data = array();
        $data['EPS_CARDNUMBER'] = $this->getCard()->getNumber();
        $data['EPS_EXPIRYMONTH'] = $this->getCard()->getExpiryMonth();
        $data['EPS_EXPIRYYEAR'] = $this->getCard()->getExpiryYear();
        $data['EPS_CCV'] = $this->getCard()->getCvv();

        return $data;
    }
    
}
