<?php

namespace Omnipay\NABTransact\Message;

use Omnipay\Common\Message\AbstractRequest;

/**
 * NABTransact Direct Post Abstract Request
 */
abstract class DirectPostAbstractRequest extends AbstractRequest
{
    public $testEndpoint = 'https://transact.nab.com.au/test/directpostv2/authorise';
    public $liveEndpoint = 'https://transact.nab.com.au/live/directpostv2/authorise';

    public function getMerchantId()
    {
        return $this->getParameter('merchantId');
    }

    public function setMerchantId($value)
    {
        return $this->setParameter('merchantId', $value);
    }

    public function getTransactionPassword()
    {
        return $this->getParameter('transactionPassword');
    }

    public function setTransactionPassword($value)
    {
        return $this->setParameter('transactionPassword', $value);
    }

    public function getEndpoint()
    {
        return $this->getTestMode() ? $this->testEndpoint : $this->liveEndpoint;
    }
    
    public function getDoFetchToken()
    {
    	return $this->getParameter('doFetchToken');
    }
    
    public function setDoFetchToken($value)
    {
    	return $this->setParameter('doFetchToken', $value);
    }
}
