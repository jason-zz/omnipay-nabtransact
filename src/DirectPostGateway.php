<?php

namespace Omnipay\NABTransact;

use Omnipay\Common\AbstractGateway;

/**
 * NABTransact Direct Post Gateway
 *
 * @link http://transact.nab.com.au/upop/UPOP_by_NAB_Transact_Getting_Started.pdf
 */
class DirectPostGateway extends AbstractGateway
{
    public $transparentRedirect = true;

    public function getName()
    {
        return 'NABTransact Direct Post';
    }

    public function getDefaultParameters()
    {
        return array(
            'merchantId' => '',
            'transactionPassword' => '',
            'testMode' => false,
        );
    }

    public function getMerchantId()
    {
        return $this->getParameter('merchantId');
    }

    public function setMerchantId($value)
    {
        return $this->setParameter('merchantId', $value);
    }

    public function getTransactionPassword()
    {
        return $this->getParameter('transactionPassword');
    }

    public function setTransactionPassword($value)
    {
        return $this->setParameter('transactionPassword', $value);
    }
    
    public function getDoFetchToken()
    {
    	return $this->getParameter('doFetchToken');
    }
    
    public function setDoFetchToken($value)
    {
    	return $this->setParameter('doFetchToken', $value);
    }

    public function authorize(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\NABTransact\Message\DirectPostAuthorizeRequest', $parameters);
    }

    public function completeAuthorize(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\NABTransact\Message\DirectPostCompletePurchaseRequest', $parameters);
    }

    public function purchase(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\NABTransact\Message\DirectPostPurchaseRequest', $parameters);
    }

    public function completePurchase(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\NABTransact\Message\DirectPostCompletePurchaseRequest', $parameters);
    }
    
    public function createToken(array $parameters = array())
    {
    	return $this->createRequest('\Omnipay\NABTransact\Message\DirectPostCreateTokenRequest', $parameters);
    }
    
    public function completeCreateToken(array $parameters = array())
    {
    	return $this->createRequest('\Omnipay\NABTransact\Message\DirectPostCompleteCreateTokenRequest', $parameters);
    }
    
}
